Version 4
SymbolType BLOCK
LINE Normal 0 8 8 0
LINE Normal 0 -8 0 8
LINE Normal 8 0 0 -8
LINE Normal 8 -8 8 0
LINE Normal 11 -12 8 -8
LINE Normal 8 -8 11 -12
LINE Normal 8 8 8 -8
LINE Normal 6 11 8 8
LINE Normal 8 8 6 11
LINE Normal 8 0 8 8
LINE Normal 32 0 8 0
LINE Normal -32 0 32 0
RECTANGLE Normal -32 -24 32 24
TEXT 13 12 Left 2 C
TEXT -30 13 Left 2 A
WINDOW 0 0 -21 Bottom 2
WINDOW 3 0 24 Top 2
SYMATTR Value mmsz4678t1g
SYMATTR Prefix X
SYMATTR ModelFile .\MMSZ4678T1G.LIB
PIN -32 0 NONE 8
PINATTR PinName 2
PINATTR SpiceOrder 1
PIN 32 0 NONE 8
PINATTR PinName 1
PINATTR SpiceOrder 2
