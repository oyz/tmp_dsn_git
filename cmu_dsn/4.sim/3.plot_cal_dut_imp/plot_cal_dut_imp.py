# plot_cal_dut_imp.py
#
#  plot Calibration DUT impedence (magnitude and angle)
#
#  DUT impedence model:
#
#    Rs Rp Cp
#
#  abused notation:
#    Z_dut = Rs + Rp // Cp
#    Z_dut = Rs + Rp // ( 1/ (j * 2* pi * Cp) )
#
#  Laplace form:
#    Z_dut = Rs + 1 / ( 1/Rp + (j * 2* pi * f * Cp) )
#

import numpy as np
import matplotlib.pyplot as plt
plt.ion() # matplotlib interactive mode 

# test impedence calculation
############################################
#Rs1    = 34.8 # ohm 
#Rp1    = 21.5e3 # ohm
#Cp1    = 150e-12 # Farad
#Rs1    = 33.2 # ohm 
#Rp1    = 100e3 # ohm
#Cp1    = 0.033e-9 # Farad
#############################################
##Rs2    = 34.8 # ohm 
##Rp2    = 21.5e3 # ohm
##Cp2    = 150e-12 # Farad
#Rs2    = 20 # ohm 
#Rp2    = 22e3 # ohm
#Cp2    = 0.1e-9 # Farad
#############################################
##Rs3    = 34.8 # ohm 
##Rp3    = 21.5e3 # ohm
##Cp3    = 150e-12 # Farad
#Rs3    = 10 # ohm 
#Rp3    = 3.3e3 # ohm
#Cp3    = 0.47e-9 # Farad
#############################################

# option
#dut_test = [[3.3, 100e3, 0.033e-9],[2.0, 22e3, 0.1e-9],[0.22, 3.3e3, 0.47e-9]]
#dut_test = [[33, 1000e3, 0.0033e-9],[22.0, 220e3, 0.01e-9],[1.1, 33.0e3, 0.047e-9]]
dut_test = [[33, 600e3, 0.0047e-9],[22.0, 220e3, 0.01e-9],[1.1, 33.0e3, 0.047e-9]]
#
#dut_1MHz = [[33.2, 100e3, 0.033e-9],[20, 22e3, 0.1e-9],[10, 3.3e3, 0.47e-9]]
#dut_1MHz = [[3.3, 100e3, 0.33e-9],[2.0, 22e3, 1.0e-9],[0.22, 3.3e3, 4.7e-9]]
dut_1MHz = [[33, 1000e3, 0.033e-9],[22.0, 220e3, 0.1e-9],[1.1, 33.0e3, 0.47e-9]]
#
#dut_100kHz = [[33.2, 100e3, 0.33e-9],[20, 22e3, 1.0e-9],[10, 3.3e3, 4.7e-9]]
#dut_100kHz = [[3.3, 100e3, 3.3e-9],[2.0, 22e3, 10.0e-9],[0.22, 3.3e3, 47.0e-9]]
dut_100kHz = [[33, 1000e3, 0.33e-9],[22.0, 220e3, 1.0e-9],[1.1, 33.0e3, 4.7e-9]]
#
#dut_10kHz = [[33.2, 100e3, 3.3e-9],[20, 22e3, 10.0e-9],[10, 3.3e3, 47.0e-9]]
#dut_10kHz = [[3.3, 100e3, 33.0e-9],[2.0, 22e3, 100.0e-9],[0.22, 3.3e3, 470.0e-9]]
dut_10kHz = [[33, 1000e3, 3.3e-9],[22.0, 220e3, 10.0e-9],[1.1, 33.0e3, 47.0e-9]]
#
dut_1kHz = [[33, 1000e3, 33.0e-9],[22.0, 220e3, 100.0e-9],[1.1, 33.0e3, 470.0e-9]]

##
# DUT use ##
# Rs: 33,   22, 1.1 ohm
# Rp: 1G, 220k, 33k ohm
# Cp:  33pF, 100pF, 470pF
#     330pF,   1nF, 4.7nF
#     3.3nF,  10nF,  47nF
#      33nF, 100nF, 470nF


# choice
dut = dut_test
#dut = dut_1MHz
#dut = dut_100kHz
#dut = dut_10kHz
#dut = dut_1kHz

# png file name 
filename_png_pre = 'plot_dut_test_'
#filename_png_pre = 'plot_dut_1MHz_'
#filename_png_pre = 'plot_dut_100kHz_'
#filename_png_pre = 'plot_dut_10kHz_'
#filename_png_pre = 'plot_dut_1kHz_'


# assign 
Rs1 = dut[0][0]
Rp1 = dut[0][1]
Cp1 = dut[0][2]
Rs2 = dut[1][0]
Rp2 = dut[1][1]
Cp2 = dut[1][2]
Rs3 = dut[2][0]
Rp3 = dut[2][1]
Cp3 = dut[2][2]

#ft    = 100e3 # Hz
#Zd    = Rs + 1 / ( 1/Rp + (1j * 2* np.pi * ft * Cp) ) # ohm 
#print('{} = {}'.format('Rs',Rs))
#print('{} = {}'.format('Rp',Rp))
#print('{} = {}'.format('Cp',Cp))
#print('{} = {}'.format('ft',ft))
#print('{} = {}'.format('Zd',Zd))

# test freq sweep 

#ft_list = [1e3, 2e3, 5e3, 10e3, 20e3, 50e3, 80e3, 90e3, 100e3, 110e3, 120e3, 200e3, 500e3, 1000e3, 2000e3, 5000e3, 10000e3]
#ft_list = [ff  for ff in np.arange(100,100e6,100)]
#ft_list = [ff  for ff in np.arange(1000,100e6,1000)]
#ft_list = [ff  for ff in np.arange(1000,100e6,200)]
#ft_list = [ff  for ff in np.arange(100,1000e6,200)]
#ft_list = [ff  for ff in np.logspace(2.0, 8.0, 100)]
ft_list = [ff  for ff in np.logspace(0.0, 9.0, 100)]
#
Zd1_list = [Rs1 + 1 / ( 1/Rp1 + (1j * 2* np.pi * ff * Cp1) )  for ff in ft_list]
Zd2_list = [Rs2 + 1 / ( 1/Rp2 + (1j * 2* np.pi * ff * Cp2) )  for ff in ft_list]
Zd3_list = [Rs3 + 1 / ( 1/Rp3 + (1j * 2* np.pi * ff * Cp3) )  for ff in ft_list]

#print('{} = {}'.format('ft_list',ft_list))
#print('{} = {}'.format('Zd1_list',Zd1_list))
#print('{} = {}'.format('Zd2_list',Zd2_list))
#print('{} = {}'.format('Zd3_list',Zd3_list))

label1 = 'Rs={:0.2f}[Ohm],Rp={:0.1f}[kOhm],Cp={:0.4f}[nF]'.format(Rs1,Rp1/1e3,Cp1*1e9)
label2 = 'Rs={:0.2f}[Ohm],Rp={:0.1f}[kOhm],Cp={:0.3f}[nF]'.format(Rs2,Rp2/1e3,Cp2*1e9)
label3 = 'Rs={:0.2f}[Ohm],Rp={:0.1f}[kOhm],Cp={:0.3f}[nF]'.format(Rs3,Rp3/1e3,Cp3*1e9)


## data for plot
#
x_freq_list = ft_list
#
y1_mag_list = [np.absolute(yy)         for yy in Zd1_list]
y2_mag_list = [np.absolute(yy)         for yy in Zd2_list]
y3_mag_list = [np.absolute(yy)         for yy in Zd3_list]
#
y1_ang_list = [np.angle(yy, deg=True)  for yy in Zd1_list]
y2_ang_list = [np.angle(yy, deg=True)  for yy in Zd2_list]
y3_ang_list = [np.angle(yy, deg=True)  for yy in Zd3_list]
#
x1_real_list = [np.real(yy)         for yy in Zd1_list]
x2_real_list = [np.real(yy)         for yy in Zd2_list]
x3_real_list = [np.real(yy)         for yy in Zd3_list]
#
y1_imag_list = [np.imag(yy)         for yy in Zd1_list]
y2_imag_list = [np.imag(yy)         for yy in Zd2_list]
y3_imag_list = [np.imag(yy)         for yy in Zd3_list]
#
####


# plot 
fig, ax1 = plt.subplots(figsize=(15,10)) ###

plt.suptitle('Calibration DUT impedence (mag,phase) vs Freq (' + filename_png_pre + ')')

color = 'tab:red'
linestyle = '-'
ax1.set_xlabel('Freq[Hz]')
ax1.set_ylabel('Mag(Z)[Ohm]', color=color)
ax1.set_yscale('log')
ax1.tick_params(axis='y', colors=color)
ax1.grid(color=color, linestyle=linestyle, linewidth=0.1)

ax1.semilogx(x_freq_list,y1_mag_list, linestyle, alpha=0.75, label=label1)
ax1.semilogx(x_freq_list,y2_mag_list, linestyle, alpha=0.75, label=label2)
ax1.semilogx(x_freq_list,y3_mag_list, linestyle, alpha=0.75, label=label3)

ax1.legend()


ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis

color = 'tab:blue'
linestyle = ':'
ax2.set_ylabel('Angle(Z)[Degree]', color=color)
ax2.set_ylim([-90,None]) 
ax2.tick_params(axis='y', colors=color)
ax2.grid(color=color, linestyle=linestyle, linewidth=1.0)

ax2.semilogx(x_freq_list,y1_ang_list, linestyle, alpha=0.75, label=label1)
ax2.semilogx(x_freq_list,y2_ang_list, linestyle, alpha=0.75, label=label2)
ax2.semilogx(x_freq_list,y3_ang_list, linestyle, alpha=0.75, label=label3)

#ax2.legend()

#plt.grid(True)

plt.savefig(filename_png_pre+'0.png') 


####
#input('')


# plot 
FIG_NUM = None
fig = plt.figure(FIG_NUM,figsize=(15,10))
plt.suptitle('Calibration DUT impedence vs Freq (' + filename_png_pre + ')')

plt.subplot(221) ### 
plt.semilogx(x_freq_list,y1_mag_list, alpha=0.75, label=label1)
plt.semilogx(x_freq_list,y2_mag_list, alpha=0.75, label=label2)
plt.semilogx(x_freq_list,y3_mag_list, alpha=0.75, label=label3)
plt.legend()
plt.ylabel('Mag(Z)[Ohm]')
plt.xlabel('Freq[Hz]')
plt.grid(True)
plt.yscale('log')

plt.subplot(222) ### 
plt.semilogx(x_freq_list,y1_ang_list, alpha=0.75, label=label1)
plt.semilogx(x_freq_list,y2_ang_list, alpha=0.75, label=label2)
plt.semilogx(x_freq_list,y3_ang_list, alpha=0.75, label=label3)
plt.legend()
plt.ylabel('Angle(Z)[Degree]')
plt.xlabel('Freq[Hz]')
plt.grid(True)

plt.subplot(223) ### 
plt.plot(x1_real_list,y1_imag_list, alpha=0.75, label=label1)
plt.plot(x2_real_list,y2_imag_list, alpha=0.75, label=label2)
plt.plot(x3_real_list,y3_imag_list, alpha=0.75, label=label3)
plt.legend()
plt.ylabel('imag(Z)[Ohm]')
plt.xlabel('real(Z)[Ohm]')
plt.grid(True)

plt.savefig(filename_png_pre+'1.png') 

####
#input('')

##
plt.figure(figsize=(15,10))
plt.plot(x_freq_list,y1_mag_list, alpha=0.75, label=label1)
plt.plot(x_freq_list,y2_mag_list, alpha=0.75, label=label2)
plt.plot(x_freq_list,y3_mag_list, alpha=0.75, label=label3)
plt.legend()
plt.ylabel('Mag(Z)[Ohm]')
plt.xlabel('Freq[Hz]')
plt.grid(True)
plt.savefig(filename_png_pre+'2.png') 

##
plt.figure(figsize=(15,10))
plt.plot(x_freq_list,y1_ang_list, alpha=0.75, label=label1)
plt.plot(x_freq_list,y2_ang_list, alpha=0.75, label=label2)
plt.plot(x_freq_list,y3_ang_list, alpha=0.75, label=label3)
plt.legend()
plt.ylabel('Angle(Z)[Degree]')
plt.xlabel('Freq[Hz]')
plt.grid(True)
plt.savefig(filename_png_pre+'3.png') 

##
plt.figure(figsize=(15,10))
plt.plot(x1_real_list,y1_imag_list, alpha=0.75, label=label1)
plt.plot(x2_real_list,y2_imag_list, alpha=0.75, label=label2)
plt.plot(x3_real_list,y3_imag_list, alpha=0.75, label=label3)
plt.legend()
plt.ylabel('imag(Z)[Ohm]')
plt.xlabel('real(Z)[Ohm]')
plt.grid(True)
plt.savefig(filename_png_pre+'4.png') 


####################################################
