# sim_8F4LVL.py
#
#
# 4 levels:
#    1, 2/3, 1/3, 0
#    1, 3/4, 2/4, 1/4
#    4/5, 3/5, 2/5, 1/5 ... 4 3 2 1
#    1, 4/5, 2/5, 1/5 ... 5 4 2 1
#    5/6, 4/6, 2/6, 1/6 ... 5 4 2 1
#
# case1)
#	.... VDD -+- R0 -+------R1 ---+--- OUT
#	          +- R0 -|-+----R2 ---+
#             +- R0 -|-|-+--R3 ---+
#	                 | | |
#	                 +-|-|--S1-- GND
#	                   +-|--S2-- GND
#	                     +--S3-- GND
#
#	0	0  0  0  VDD = 1
#	1	1  0  0  VDD*R1/(((R0+R2)//(R0+R3))+R1) = 2/3
#	2	1  1  0  VDD*(R1//R2)/((R0+R3)+(R1//R2)) = 1/3
#	3	1  1  1  GND = 0
#
# result : [R1, R2, R3] = [(1.5*R0, R0, 0.2*R0)]
#                       = [(750, 500, 100)]
#
#  R0:R1:R2:R3=10:15:10:2
#
# S1 = R1/(( para_sum((R0+R2),(R0+R3)) )+R1) - 3/4
# S2 = para_sum(R1,R2)/((R0+R3)+para_sum(R1,R2)) - 1/4
# S3 = R2 - 0.75*R0 - 0
# for [(2.625*R0, 0.75*R0, 0.75*R0)]
#  R0:R1:R2:R3=4:10.5:3:3 = 800:2100:600:600
#
# case2)
#	.... VDD --- R0 --- OUT ---+--- R1 ---S1-- GND
#	                           +--- R2 ---S2-- GND
#	                           +--- R3 ---S3-- GND
#
#	0	0  0  0  VDD = 1
#	1	1  0  0  VDD*R1/(R0+R1) = 3/4 
#	2	1  1  0  VDD*(R1//R2)/(R0+(R1//R2)) = 2/4 
#	3	1  1  1  VDD*(R1//R2//R3)/(R0+(R1//R2//R3)) = 1/4 
#
#
# result : [R1, R2, R3] = [(3.0*R0, 1.5*R0, 0.5*R0)]
#                       = [(600, 300, 100)]
#
#  R0:R1:R2:R3=2:6:3:1
#
# S1 = R1/(R0+R1) - 3/4
# S2 = para_sum(R1,R2)/(R0+para_sum(R1,R2)) - 1/4
# R3 = 0
# for [(3.0*R0, 0.375*R0)]
#  R0:R1:R2:R3=24:72:9:0 = 480:1440:180:0
# note 60 degree phase points .. 6f waveform
#
# case3)
#	.... VDD --- R0 -+- OUT ---+--- R1 ---S1-- GND
#	                 |         +--- R2 ---S2-- GND
#	                 |         +--- R3 ---S3-- GND
#	     GND --- R4 -+
#
#  (VDD-OUT)/R0 = OUT/R1 + OUT/R2 + OUT/R3 + OUT/R4
#  VDD/R0 = OUT/R0 + OUT/R1 + OUT/R2 + OUT/R3 + OUT/R4
#  OUT = (1/R0)/(1/R0 + 1/R1 + 1/R2 + 1/R3 + 1/R4)*VDD
#
#	0	0  0  0  VDD/R0*(R0//R4) - 5/6
#	1	1  0  0  VDD/R0*(R0//R1//R4) - 4/6
#	2	1  1  0  VDD/R0*(R0//R1//R2//R4) - 2/6
#	3	1  1  1  VDD/R0*(R0//R1//R2//R3//R4) - 1/6
#

#####################
# from sympy import *
# >>> x = Symbol('x')
# >>> y = Symbol('y')
#
# In [8]: solve([x + 5*y - 2, -3*x + 6*y - 15], [x, y])
# Out[8]: {y: 1, x: -3}

#########
# https://matplotlib.org/gallery/lines_bars_and_markers/spectrum_demo.html

## install modules ... on cmd under admin
# pip install sympy
# pip install numpy (option)
# pip install matplotlib

# from sympy import *

# R0 = Symbol('R0')
# R1 = Symbol('R1')
# R2 = Symbol('R2')
# R3 = Symbol('R3')
# R4 = Symbol('R4')

## libraries
import numpy as np
import matplotlib.pyplot as plt
#
plt.ion() # matplotlib interactive mode
#plt.close('all')


## functions
#def para_sum(x,y):
#    r = 1 / (1/x + 1/y)
#    return r
def para_sum(x,*z):
    if len(z) == 0:
        r = x
    #if len(z) == 1:
    #    y = z[0]
    #    r = 1 / (1/x + 1/y)
    else:
        r = 1 / (1/x + 1/para_sum(*z))
    return r


## 8f-4level parameters setting
print('>>{}'.format('8f-4level parameters setting'))
# board init condition: 1f-3f 50dB harmonics
R0 = 500
R1 = 2320 
R2 = 1070
R3 = 866
R4 = 2000
#
print('{} = {}'.format('R0',R0))
print('{} = {}'.format('R1',R1))
print('{} = {}'.format('R2',R2))
print('{} = {}'.format('R3',R3))
print('{} = {}'.format('R4',R4))
#
S0 = para_sum(R0,R4)/R0  # = R0*R4/(R0+R4)/R0 = R4/(R0+R4)
S1 = para_sum(R0,R1,R4)/R0
S2 = para_sum(R0,R1,R2,R4)/R0
S3 = para_sum(R0,R1,R2,R3,R4)/R0
#
print('{} = {}'.format('S0',S0))
print('{} = {}'.format('S1',S1))
print('{} = {}'.format('S2',S2))
print('{} = {}'.format('S3',S3))
#
VDD=3.3
L0 = VDD*S0
L1 = VDD*S1
L2 = VDD*S2
L3 = VDD*S3
#
#L1 = (L0*3+L3)/4
#L2 = (L0+L3*3)/4
#
print('{} = {}'.format('L0',L0))
print('{} = {}'.format('L1',L1))
print('{} = {}'.format('L2',L2))
print('{} = {}'.format('L3',L3))
#
D1 = L0-L1
D2 = L1-L2
D3 = L2-L3
#
print('{} = {}'.format('D1',D1))
print('{} = {}'.format('D2',D2))
print('{} = {}'.format('D3',D3))
#
HR1 = (L0-L1)/(L0-L3)
HR2 = (L1-L2)/(L0-L3)
HR3 = (L2-L3)/(L0-L3)
#
print('{} = {}'.format('HR1',HR1))
print('{} = {}'.format('HR2',HR2))
print('{} = {}'.format('HR3',HR3))


## 8f-4level waveform calculate
print('>>{}'.format('8f-4level waveform calculate'))
#  1  2  3  4  5  6  7  8
#  L1-L0-L0-L1-L2-L3-L3-L2
#  
# test signal freq ######################################
#Ft = 10e6 # Hz ... 8f-4level good
#Ft = 2e6 # Hz ... pwm-4level good
#Ft = 200e3 # Hz
Ft = 20e3 # Hz
print('{} = {}'.format('Ft',Ft))
# data sampling freq
#Fs = Ft*8*100
Fs = Ft*8*100
#Fs = 200e6 # Hz
print('{} = {}'.format('Fs',Fs))
# number of periods to see
num_periods = 300
# number of sampling in a test signal period 
num_samples_in_a_cycle = int(Fs/Ft)
print('{} = {}'.format('num_samples_in_a_cycle',num_samples_in_a_cycle))
#
# num_repeat_in_a_level
num_repeat_in_a_level = int(num_samples_in_a_cycle/8)
print('{} = {}'.format('num_repeat_in_a_level',num_repeat_in_a_level))
#
# make a pattern
list_float = [float(L1)]*num_repeat_in_a_level \
			+[float(L0)]*num_repeat_in_a_level \
			+[float(L0)]*num_repeat_in_a_level \
			+[float(L1)]*num_repeat_in_a_level \
			+[float(L2)]*num_repeat_in_a_level \
			+[float(L3)]*num_repeat_in_a_level \
			+[float(L3)]*num_repeat_in_a_level \
			+[float(L2)]*num_repeat_in_a_level 
# see https://stackoverflow.com/questions/3459098/create-list-of-single-item-repeated-n-times-in-python
#
# repeat the pattern
x = list_float * num_periods
#print('{} = {}'.format('list_float',list_float))
#
# make a noise z
z_mean = 0;
#z_std = 0.001 
z_std = 0.005 
z_len = len(x)
z = np.random.normal(z_mean,z_std,z_len)
# 0 is the mean of the normal distribution you are choosing from
# 1 is the standard deviation of the normal distribution
# 100 is the number of elements you get in array noise
#
# change x into numpy array
x = np.asarray(x).reshape(len(x))
#
# add the noise to x 
x = x + z
#
# make a filter y for x 
#  see https://en.wikipedia.org/wiki/Low-pass_filter
def filter_iir_alpha(x,alpha,Fs,DEBUG=True):
	#y = [float]*len(x)
	y = np.zeros(len(x))
	#alpha=0.001 # 
	#alpha=0.01 # 
	#alpha=0.02 # 
	# time_constant = t_sample * (1-alpha) / alpha
	# t_sample = 1/Fs
	time_constant = (1/Fs) * (1-alpha) / alpha
	if DEBUG: print('{} = {}'.format('time_constant[s]',time_constant))
	# 3dB cut-off freq = 2*pi*(1/time_constant)
	freq_cutoff = (2*np.pi*(1/time_constant)/1e6) # MHz
	if DEBUG: print('{} = {}'.format('freq_cutoff[MHz]',freq_cutoff))
	#
	for ii in range(len(x)):
		if ii == 0:
			y[ii] = (alpha)*x[ii]
		else:
			y[ii] = (alpha)*x[ii] + (1-alpha)*y[ii-1]
	return y
#
#freq_cutoff = 20e6 # Hz
#freq_cutoff = 2e6 # Hz
freq_cutoff = 1.6e6 # Hz
#
time_constant = 2*np.pi/freq_cutoff # s
alpha = 1 /(time_constant * Fs + 1 )
#alpha = 0.02 # 2MHz cutoff
#alpha = 0.01 # 1MHz cutoff
#alpha = 0.0005 # 
#alpha = 0.002 # 200kHz cutoff
y = filter_iir_alpha (x,alpha,Fs)


## pwm-4level waveform calculate
print('>>{}'.format('pwm-4level waveform calculate'))
# use the same levels 
#  voltages: L0 L1 L2 L3
print('{} = {}'.format('L0',L0))
print('{} = {}'.format('L1',L1))
print('{} = {}'.format('L2',L2))
print('{} = {}'.format('L3',L3))

# generate sin wave 
#  target test signal freq Ft
#  data sampling rate Fs
num_samples = len(x)
time_finish = 1/Fs*(num_samples-1)
print('{} = {}'.format('Ft',Ft))
print('{} = {}'.format('Fs',Fs))
print('{} = {}'.format('num_samples',num_samples))
print('{} = {}'.format('num_samples_in_a_cycle',num_samples_in_a_cycle))
#
t = np.linspace(0,time_finish,num_samples)
s_mean = (L0+L1+L2+L3)/4
s_amp = (L0-L3)/2*0.999
s = s_amp*np.sin(2*np.pi*Ft*t)+s_mean

# generate triangular waves
#  reference triangular wave freq ... Fr
#  tri_wave_1 ... between L0 and L1
#  tri_wave_2 ... between L1 and L2
#  tri_wave_3 ... between L2 and L3
#
#Fr = 160e6
#Fr = 80e6 
#Fr = 60e6
#Fr = 40e6
#Fr = 20e6
Fr = Ft*80 # note that 80x pulse switching freq over signal freq.
print('{} = {}'.format('Fr',Fr))
print('{} = {}'.format('Fs/Fr',Fs/Fr))
print('{} = {}'.format('1/Fr',1/Fr))
print('{} = {}'.format('1/Fs',1/Fs))
#
# function for generating triangular waveform
def func_triang(time,period,level_high=1,level_low=0):
	t_mod = np.mod(time,period)
	#
	# saw
	#ret = t_mod/period*(level_high-level_low)+level_low 
	#
	# centered triangle ... vector-style
	ret1 = t_mod/period*(level_high-level_low)*2+level_low 
	ret2 = t_mod/period*(level_low-level_high)*2+level_high*2-level_low
	ret1[(ret1>level_high)] = 0
	ret2[(ret2>level_high)] = 0
	ret = ret1 + ret2
	ret[(ret>level_high)] = level_high
	return ret
#
tri_wave_1 = func_triang(t,1/Fr,L0,L1)
tri_wave_2 = func_triang(t,1/Fr,L1,L2)
tri_wave_3 = func_triang(t,1/Fr,L2,L3)

# compare the t-wave and the sin-wave for pulse modulation
#
# function for switching output by comparison
def func_compare(w_target, w_ref,level_high=1,level_low=0):
	ret = np.zeros(len(w_target))
	idx_large = (w_target>w_ref)
	ret[idx_large] = level_high
	ret[np.logical_not(idx_large)] = level_low
	return ret
#
s_modulated1 = func_compare(s,tri_wave_1,L0,L1)
s_modulated2 = func_compare(s,tri_wave_2,L1,L2)
s_modulated3 = func_compare(s,tri_wave_3,L2,L3)

# merge pulse modulation
#  triw1  H  L  L  L   (triw1>L1)
#  triw2  X  H  L  L   (triw2>L2)
#  triw3  X  X  H  L   (triw3>L3)
#  merge..L0 L1 L2 L3 
def func_merge_pwm_4level(triw1,triw2,triw3,L0,L1,L2,L3):
	ret = np.zeros(len(triw1))
	idx_H_triw1 = (triw1>L1)
	idx_H_triw2 = (triw2>L2)
	idx_H_triw3 = (triw3>L3)
	idx_L_triw1 = np.logical_not(idx_H_triw1)
	idx_L_triw2 = np.logical_not(idx_H_triw2)
	idx_L_triw3 = np.logical_not(idx_H_triw3)
	# determine levels
	idx_det_L0 = idx_H_triw1
	idx_det_L1 = np.logical_and(idx_L_triw1,idx_H_triw2)
	idx_det_L2 = np.logical_and(np.logical_and(idx_L_triw1,idx_L_triw2),idx_H_triw3)
	idx_det_L3 = np.logical_and(np.logical_and(idx_L_triw1,idx_L_triw2),idx_L_triw3)
	# set levels
	ret[idx_det_L0] = L0
	ret[idx_det_L1] = L1
	ret[idx_det_L2] = L2
	ret[idx_det_L3] = L3
	return ret
#
s_modulated = func_merge_pwm_4level(s_modulated1,s_modulated2,s_modulated3,L0,L1,L2,L3)

# add the same noise
s_modulated = s_modulated + z

# filter
#  use same alpha
s_modulated_filt = filter_iir_alpha (s_modulated,alpha,Fs)

## pwm-4level waveform plot
print('>>{}'.format('pwm-4level waveform plot'))
#
plt.figure(2)
#
plt_time_scale = 1e6
plt_time_scale_txt = r'time[${\mu}s$]'
plt_mask = (t>1/Ft*10) & (t<1/Ft*11)
#
plt.subplot(221) ###
#
plt.plot([t[plt_mask][0]*plt_time_scale,t[plt_mask][-1]*plt_time_scale], [L0,L0], 'g:') # level L0
plt.plot([t[plt_mask][0]*plt_time_scale,t[plt_mask][-1]*plt_time_scale], [L1,L1], 'g:') # level L1
plt.plot([t[plt_mask][0]*plt_time_scale,t[plt_mask][-1]*plt_time_scale], [L2,L2], 'g:') # level L2
plt.plot([t[plt_mask][0]*plt_time_scale,t[plt_mask][-1]*plt_time_scale], [L3,L3], 'g:') # level L3
#
plt.plot(t[plt_mask]*plt_time_scale, tri_wave_1[plt_mask], 'g-')
plt.plot(t[plt_mask]*plt_time_scale, tri_wave_2[plt_mask], 'y-')
plt.plot(t[plt_mask]*plt_time_scale, tri_wave_3[plt_mask], 'c-')
#
plt.plot(t[plt_mask]*plt_time_scale, s[plt_mask], 'r-')
#
plt.plot(t[plt_mask]*plt_time_scale, s_modulated1[plt_mask], 'b-')
plt.plot(t[plt_mask]*plt_time_scale, s_modulated2[plt_mask], 'b-')
plt.plot(t[plt_mask]*plt_time_scale, s_modulated3[plt_mask], 'b-')
#
plt.title('pwm-4level generation over target sine wave')
plt.xlabel(plt_time_scale_txt)
plt.ylabel('voltage[V]')
plt.grid(True)
plt.autoscale()
#
plt.subplot(222) ###
#
#plt.plot(t*1e3, s_modulated1, 'r-')
#plt.plot(t*1e3, s_modulated2, 'g-')
#plt.plot(t*1e3, s_modulated3, 'b-')
plt.plot(t[plt_mask]*plt_time_scale, s_modulated[plt_mask],  'k:')
plt.plot(t[plt_mask]*plt_time_scale, x[plt_mask],  'b-')
plt.plot(t[plt_mask]*plt_time_scale, s[plt_mask],  'r-')
#
plt.title('target sine wave vs pwm-4level wave vs 8f-4level')
plt.xlabel(plt_time_scale_txt)
plt.ylabel('voltage[V]')
plt.grid(True)
plt.autoscale()
#
plt.subplot(223) ###
plt.psd(s_modulated, len(s_modulated), Fs)
plt.title('\nPSD of pwm-4level sine wave')
plt.grid(True)
plt.autoscale()
#plt.xlim(0, 100e6)
#
plt.subplot(224) ###
plt.psd(x, len(x), Fs)
plt.title('\nPSD of 8f-4level')
#plt.grid(True)
plt.autoscale()
#plt.xlim(0, 100e6)





#
plt.figure(3)
#
plt_time_scale = 1e6
plt_time_scale_txt = r'time[${\mu}s$]'
#
plt.subplot(241) ###
#
plt_mask = (t>1/Ft*130) & (t<1/Ft*133)
plt.plot(t[plt_mask]*plt_time_scale, s[plt_mask],  'r-')
plt.plot(t[plt_mask]*plt_time_scale, s_modulated_filt[plt_mask],  'k-')
#
plt.title('target sine wave vs 4level-pwm sine wave filtered')
plt.xlabel(plt_time_scale_txt)
plt.ylabel('voltage[V]')
plt.grid(True)
plt.autoscale()
#
plt.subplot(242) ###
#
plt_mask = (t>1/Ft*10) & (t<1/Ft*13)
plt.plot(t[plt_mask]*plt_time_scale, s[plt_mask],  'r-')
plt.plot(t[plt_mask]*plt_time_scale, s_modulated[plt_mask],  'k-')
#
plt.title('target sine wave vs 4level-pwm sine wave')
plt.xlabel(plt_time_scale_txt)
plt.ylabel('voltage[V]')
plt.grid(True)
plt.autoscale()
#
plt.subplot(243) ###
#
plt_mask = (t>1/Ft*10) & (t<1/Ft*13)
plt.plot(t[plt_mask]*plt_time_scale, s[plt_mask],  'r-')
plt.plot(t[plt_mask]*plt_time_scale, x[plt_mask],  'k-')
#
plt.title('target sine wave vs 8f-4level sine wave')
plt.xlabel(plt_time_scale_txt)
plt.ylabel('voltage[V]')
plt.grid(True)
plt.autoscale()
#
plt.subplot(244) ###
#
plt_mask = (t>1/Ft*10) & (t<1/Ft*13)
plt.plot(t[plt_mask]*plt_time_scale, s[plt_mask],  'r-')
plt.plot(t[plt_mask]*plt_time_scale, y[plt_mask],  'k-')
#
plt.title('target sine wave vs 8f-4level sine wave filtered')
plt.xlabel(plt_time_scale_txt)
plt.ylabel('voltage[V]')
plt.grid(True)
plt.autoscale()
#
#
plt.subplot(245) ###
plt.psd(s_modulated_filt, len(s_modulated_filt), Fs)
plt.title('\nPSD of 4level-pwm sine wave filtered')
plt.grid(True)
plt.autoscale()
#plt.xlim(0, 100e6)
#
plt.subplot(246) ###
plt.psd(s_modulated, len(s_modulated), Fs)
plt.title('\nPSD of 4level-pwm sine wave')
plt.grid(True)
plt.autoscale()
#plt.xlim(0, 100e6)
#
plt.subplot(247) ###
plt.psd(x, len(x), Fs)
plt.title('\nPSD of 8f-4level sine wave')
#plt.grid(True)
plt.autoscale()
#plt.xlim(0, 100e6)
#
plt.subplot(248) ###
plt.psd(y, len(y), Fs)
plt.title('\nPSD of 8f-4level sine wave')
#plt.grid(True)
plt.autoscale()
#plt.xlim(0, 100e6)

## pwm-5level waveform calculate
print('>>{}'.format('pwm-5level waveform calculate'))
#
# new levels: L0, L1, (L1+L2)/2, L2, L3
#L0_5L = L0
#L1_5L = L1
#L2_5L = (L1+L2)/2
#L3_5L = L2
#L4_5L = L3
#
L0_5L = L0
L1_5L = (L0*3+L1*2)/5
L2_5L = (L1+L2)/2
L3_5L = (L2*2+L3*3)/5
L4_5L = L3
#
#L0_5L = L0
#L1_5L = (L0+(L1+L2)/2)/2
#L2_5L = (L1+L2)/2
#L3_5L = ((L1+L2)/2+L3)/2
#L4_5L = L3
#
tri_5L_wave_1 = func_triang(t,1/Fr,L0_5L,L1_5L)
tri_5L_wave_2 = func_triang(t,1/Fr,L1_5L,L2_5L)
tri_5L_wave_3 = func_triang(t,1/Fr,L2_5L,L3_5L)
tri_5L_wave_4 = func_triang(t,1/Fr,L3_5L,L4_5L)
#
s_5L_modulated1 = func_compare(s,tri_5L_wave_1,L0_5L,L1_5L)
s_5L_modulated2 = func_compare(s,tri_5L_wave_2,L1_5L,L2_5L)
s_5L_modulated3 = func_compare(s,tri_5L_wave_3,L2_5L,L3_5L)
s_5L_modulated4 = func_compare(s,tri_5L_wave_4,L3_5L,L4_5L)
#
def func_merge_pwm_5level(triw1,triw2,triw3,triw4,L0,L1,L2,L3,L4):
	ret = np.zeros(len(triw1))
	idx_H_triw1 = (triw1>L1)
	idx_H_triw2 = (triw2>L2)
	idx_H_triw3 = (triw3>L3)
	idx_H_triw4 = (triw4>L4)
	idx_L_triw1 = np.logical_not(idx_H_triw1)
	idx_L_triw2 = np.logical_not(idx_H_triw2)
	idx_L_triw3 = np.logical_not(idx_H_triw3)
	idx_L_triw4 = np.logical_not(idx_H_triw4)
	# determine levels
	idx_det_L0 = idx_H_triw1
	idx_det_L1 = np.logical_and(idx_L_triw1,idx_H_triw2)
	idx_det_L2 = np.logical_and(np.logical_and(
		idx_L_triw1,idx_L_triw2),idx_H_triw3)
	idx_det_L3 = np.logical_and(np.logical_and(np.logical_and(
		idx_L_triw1,idx_L_triw2),idx_L_triw3),idx_H_triw4)
	idx_det_L4 = np.logical_and(np.logical_and(np.logical_and(
		idx_L_triw1,idx_L_triw2),idx_L_triw3),idx_L_triw4)
	# set levels
	ret[idx_det_L0] = L0
	ret[idx_det_L1] = L1
	ret[idx_det_L2] = L2
	ret[idx_det_L3] = L3
	ret[idx_det_L4] = L4
	return ret
#
s_5L_modulated = func_merge_pwm_5level(
	s_5L_modulated1,s_5L_modulated2,s_5L_modulated3,s_5L_modulated4,
	L0_5L, L1_5L, L2_5L, L3_5L, L4_5L)
# add the same noise
s_5L_modulated = s_5L_modulated + z


## pwm-6level waveform calculate
print('>>{}'.format('pwm-6level waveform calculate'))
#
# new levels: L0, (L0+L1)/2, L1, L2, (L2+L3)/2, L3
#
#L0_6L = L0
#L1_6L = (L0*3+L1)/4
#L2_6L = (L1*3+L2)/4
#L3_6L = (L1+L2*3)/4
#L4_6L = (L2+L3*3)/4
#L5_6L = L3
#
L0_6L = L0
L1_6L = (L0*3+L1*2)/5
L2_6L = (L1*8+L2*2)/10
L3_6L = (L1*2+L2*8)/10
L4_6L = (L2*2+L3*3)/5
L5_6L = L3
#
tri_6L_wave_1 = func_triang(t,1/Fr,L0_6L,L1_6L)
tri_6L_wave_2 = func_triang(t,1/Fr,L1_6L,L2_6L)
tri_6L_wave_3 = func_triang(t,1/Fr,L2_6L,L3_6L)
tri_6L_wave_4 = func_triang(t,1/Fr,L3_6L,L4_6L)
tri_6L_wave_5 = func_triang(t,1/Fr,L4_6L,L5_6L)
#
s_6L_modulated1 = func_compare(s,tri_6L_wave_1,L0_6L,L1_6L)
s_6L_modulated2 = func_compare(s,tri_6L_wave_2,L1_6L,L2_6L)
s_6L_modulated3 = func_compare(s,tri_6L_wave_3,L2_6L,L3_6L)
s_6L_modulated4 = func_compare(s,tri_6L_wave_4,L3_6L,L4_6L)
s_6L_modulated5 = func_compare(s,tri_6L_wave_5,L4_6L,L5_6L)
#
def func_merge_pwm_6level(triw1,triw2,triw3,triw4,triw5,L0,L1,L2,L3,L4,L5):
	ret = np.zeros(len(triw1))
	idx_H_triw1 = (triw1>L1)
	idx_H_triw2 = (triw2>L2)
	idx_H_triw3 = (triw3>L3)
	idx_H_triw4 = (triw4>L4)
	idx_H_triw5 = (triw5>L5)
	idx_L_triw1 = np.logical_not(idx_H_triw1)
	idx_L_triw2 = np.logical_not(idx_H_triw2)
	idx_L_triw3 = np.logical_not(idx_H_triw3)
	idx_L_triw4 = np.logical_not(idx_H_triw4)
	idx_L_triw5 = np.logical_not(idx_H_triw5)
	# determine levels
	idx_det_L0 = idx_H_triw1
	idx_det_L1 = np.logical_and(idx_L_triw1,idx_H_triw2)
	idx_det_L2 = np.logical_and(np.logical_and(
		idx_L_triw1,idx_L_triw2),idx_H_triw3)
	idx_det_L3 = np.logical_and(np.logical_and(np.logical_and(
		idx_L_triw1,idx_L_triw2),idx_L_triw3),idx_H_triw4)
	idx_det_L4 = np.logical_and(np.logical_and(np.logical_and(np.logical_and(
		idx_L_triw1,idx_L_triw2),idx_L_triw3),idx_L_triw4),idx_H_triw5)
	idx_det_L5 = np.logical_and(np.logical_and(np.logical_and(np.logical_and(
		idx_L_triw1,idx_L_triw2),idx_L_triw3),idx_L_triw4),idx_L_triw5)
	# set levels
	ret[idx_det_L0] = L0
	ret[idx_det_L1] = L1
	ret[idx_det_L2] = L2
	ret[idx_det_L3] = L3
	ret[idx_det_L4] = L4
	ret[idx_det_L5] = L5
	return ret
#
s_6L_modulated = func_merge_pwm_6level(
	s_6L_modulated1,
	s_6L_modulated2,
	s_6L_modulated3,
	s_6L_modulated4,
	s_6L_modulated5,
	L0_6L, 
	L1_6L, 
	L2_6L, 
	L3_6L, 
	L4_6L,
	L5_6L)
# add the same noise
s_6L_modulated = s_6L_modulated + z


## display
#
plt.figure(4)
#
plt_time_scale = 1e6
plt_time_scale_txt = r'time[${\mu}s$]'
plt_mask = (t>1/Ft*10) & (t<1/Ft*11)
#
plt.subplot(221) ###
#
plt.plot(t[plt_mask]*plt_time_scale, s[plt_mask],  'r-')
plt.plot(t[plt_mask]*plt_time_scale, s_5L_modulated[plt_mask],  'k-')
#
plt.title('target sine wave vs 5level-pwm sine wave')
plt.xlabel(plt_time_scale_txt)
plt.ylabel('voltage[V]')
plt.grid(True)
plt.autoscale()
#
plt.subplot(222) ###
#
plt.plot(t[plt_mask]*plt_time_scale, s[plt_mask],  'r-')
plt.plot(t[plt_mask]*plt_time_scale, s_6L_modulated[plt_mask],  'k-')
#
plt.title('target sine wave vs 6level-pwm sine wave')
plt.xlabel(plt_time_scale_txt)
plt.ylabel('voltage[V]')
plt.grid(True)
plt.autoscale()
#
plt.subplot(223) ###
plt.psd(s_5L_modulated, len(s_5L_modulated), Fs)
plt.title('\nPSD of 5level-pwm sine wave')
plt.grid(True)
plt.autoscale()
#plt.xlim(0, 100e6)
#
plt.subplot(224) ###
plt.psd(s_6L_modulated, len(s_6L_modulated), Fs)
plt.title('\nPSD of 6level-pwm sine wave')
plt.grid(True)
plt.autoscale()
#plt.xlim(0, 100e6)
