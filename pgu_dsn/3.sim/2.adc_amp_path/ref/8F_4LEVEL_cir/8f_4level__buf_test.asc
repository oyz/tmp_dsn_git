Version 4
SHEET 1 6508 5620
WIRE 2816 -416 2576 -416
WIRE 3056 -416 2816 -416
WIRE 3184 -416 3184 -496
WIRE 3184 -416 3056 -416
WIRE 3248 -416 3184 -416
WIRE 3440 -416 3248 -416
WIRE 3568 -416 3440 -416
WIRE 3696 -416 3632 -416
WIRE 3744 -416 3696 -416
WIRE 3696 -384 3696 -416
WIRE 2576 -368 2576 -416
WIRE 2816 -368 2816 -416
WIRE 3056 -368 3056 -416
WIRE 3440 -352 3440 -416
WIRE 3696 -288 3696 -304
WIRE 3248 -256 3248 -416
WIRE 2816 112 2576 112
WIRE 3056 112 2816 112
WIRE 3184 112 3184 32
WIRE 3184 112 3056 112
WIRE 3248 112 3184 112
WIRE 3440 112 3248 112
WIRE 3568 112 3440 112
WIRE 3696 112 3632 112
WIRE 3744 112 3696 112
WIRE 3696 144 3696 112
WIRE 2576 160 2576 112
WIRE 2816 160 2816 112
WIRE 3056 160 3056 112
WIRE 3440 176 3440 112
WIRE 3696 240 3696 224
WIRE 3248 272 3248 112
WIRE 2752 704 2512 704
WIRE 2992 704 2752 704
WIRE 3120 704 3120 624
WIRE 3120 704 2992 704
WIRE 3184 704 3120 704
WIRE 3376 704 3184 704
WIRE 3504 704 3376 704
WIRE 3632 704 3568 704
WIRE 3872 704 3632 704
WIRE 3984 704 3936 704
WIRE 4144 704 4064 704
WIRE 4304 704 4224 704
WIRE 4400 704 4304 704
WIRE 4592 704 4400 704
WIRE 4752 704 4672 704
WIRE 3632 736 3632 704
WIRE 4304 736 4304 704
WIRE 4400 736 4400 704
WIRE 4752 736 4752 704
WIRE 2512 752 2512 704
WIRE 2752 752 2752 704
WIRE 2992 752 2992 704
WIRE 3376 768 3376 704
WIRE 3632 832 3632 816
WIRE 432 864 336 864
WIRE 576 864 432 864
WIRE 816 864 576 864
WIRE 944 864 944 784
WIRE 944 864 816 864
WIRE 1008 864 944 864
WIRE 1056 864 1008 864
WIRE 1104 864 1056 864
WIRE 1184 864 1168 864
WIRE 1232 864 1184 864
WIRE 1328 864 1232 864
WIRE 3184 864 3184 704
WIRE 4400 864 4400 816
WIRE 1184 896 1184 864
WIRE 336 912 336 864
WIRE 576 912 576 864
WIRE 816 912 816 864
WIRE 1184 992 1184 976
WIRE 1008 1152 1008 864
WIRE 2752 1344 2512 1344
WIRE 2992 1344 2752 1344
WIRE 3120 1344 3120 1264
WIRE 3120 1344 2992 1344
WIRE 3184 1344 3120 1344
WIRE 3376 1344 3184 1344
WIRE 3504 1344 3376 1344
WIRE 3632 1344 3568 1344
WIRE 3872 1344 3632 1344
WIRE 3984 1344 3936 1344
WIRE 4128 1344 4064 1344
WIRE 4304 1344 4208 1344
WIRE 4400 1344 4304 1344
WIRE 4592 1344 4400 1344
WIRE 4752 1344 4672 1344
WIRE 3632 1376 3632 1344
WIRE 4304 1376 4304 1344
WIRE 4400 1376 4400 1344
WIRE 4752 1376 4752 1344
WIRE 2512 1392 2512 1344
WIRE 2752 1392 2752 1344
WIRE 2992 1392 2992 1344
WIRE 3376 1408 3376 1344
WIRE 3632 1472 3632 1456
WIRE 3184 1504 3184 1344
WIRE 4400 1504 4400 1456
WIRE 5040 1952 4896 1952
WIRE 5152 1952 5040 1952
WIRE 5248 1952 5232 1952
WIRE 5360 1952 5248 1952
WIRE 5152 2032 5152 1952
WIRE 5184 2032 5152 2032
WIRE 5248 2032 5248 1952
WIRE 5040 2080 5040 1952
WIRE 5056 2080 5040 2080
WIRE 5168 2096 5120 2096
WIRE 5232 2096 5168 2096
WIRE 5360 2096 5360 1952
WIRE 5360 2096 5312 2096
WIRE 5456 2096 5360 2096
WIRE 5520 2096 5456 2096
WIRE 5664 2096 5600 2096
WIRE 5760 2096 5664 2096
WIRE 5824 2096 5760 2096
WIRE 5840 2096 5824 2096
WIRE 6000 2096 5904 2096
WIRE 2736 2112 2496 2112
WIRE 2976 2112 2736 2112
WIRE 3104 2112 3104 2032
WIRE 3104 2112 2976 2112
WIRE 3168 2112 3104 2112
WIRE 3360 2112 3168 2112
WIRE 3488 2112 3360 2112
WIRE 3616 2112 3552 2112
WIRE 3856 2112 3616 2112
WIRE 3968 2112 3920 2112
WIRE 4112 2112 4048 2112
WIRE 4288 2112 4192 2112
WIRE 4384 2112 4288 2112
WIRE 4576 2112 4384 2112
WIRE 4736 2112 4656 2112
WIRE 5056 2112 4736 2112
WIRE 5664 2128 5664 2096
WIRE 5760 2128 5760 2096
WIRE 3616 2144 3616 2112
WIRE 4288 2144 4288 2112
WIRE 4384 2144 4384 2112
WIRE 4736 2144 4736 2112
WIRE 2496 2160 2496 2112
WIRE 2736 2160 2736 2112
WIRE 2976 2160 2976 2112
WIRE 3360 2176 3360 2112
WIRE 3616 2240 3616 2224
WIRE 5760 2256 5760 2208
WIRE 3168 2272 3168 2112
WIRE 4384 2272 4384 2224
WIRE 5040 2736 4896 2736
WIRE 5152 2736 5040 2736
WIRE 5248 2736 5232 2736
WIRE 5360 2736 5248 2736
WIRE 5152 2816 5152 2736
WIRE 5184 2816 5152 2816
WIRE 5248 2816 5248 2736
WIRE 5040 2864 5040 2736
WIRE 5056 2864 5040 2864
WIRE 5168 2880 5120 2880
WIRE 5232 2880 5168 2880
WIRE 5360 2880 5360 2736
WIRE 5360 2880 5312 2880
WIRE 5456 2880 5360 2880
WIRE 5520 2880 5456 2880
WIRE 5616 2880 5600 2880
WIRE 5664 2880 5616 2880
WIRE 5760 2880 5664 2880
WIRE 5792 2880 5760 2880
WIRE 5840 2880 5792 2880
WIRE 6000 2880 5904 2880
WIRE 2736 2896 2496 2896
WIRE 2976 2896 2736 2896
WIRE 3104 2896 3104 2816
WIRE 3104 2896 2976 2896
WIRE 3168 2896 3104 2896
WIRE 3360 2896 3168 2896
WIRE 3488 2896 3360 2896
WIRE 3616 2896 3552 2896
WIRE 3856 2896 3616 2896
WIRE 3968 2896 3920 2896
WIRE 4112 2896 4048 2896
WIRE 4288 2896 4192 2896
WIRE 4384 2896 4288 2896
WIRE 4576 2896 4384 2896
WIRE 4736 2896 4656 2896
WIRE 5056 2896 4736 2896
WIRE 5664 2912 5664 2880
WIRE 5760 2912 5760 2880
WIRE 3616 2928 3616 2896
WIRE 4288 2928 4288 2896
WIRE 4384 2928 4384 2896
WIRE 4736 2928 4736 2896
WIRE 2496 2944 2496 2896
WIRE 2736 2944 2736 2896
WIRE 2976 2944 2976 2896
WIRE 3360 2960 3360 2896
WIRE 5616 3008 5616 2880
WIRE 3616 3024 3616 3008
WIRE 5760 3040 5760 2992
WIRE 3168 3056 3168 2896
WIRE 4384 3056 4384 3008
WIRE 2400 4032 2256 4032
WIRE 2512 4032 2400 4032
WIRE 2608 4032 2592 4032
WIRE 2720 4032 2608 4032
WIRE 2512 4112 2512 4032
WIRE 2544 4112 2512 4112
WIRE 2608 4112 2608 4032
WIRE 2400 4160 2400 4032
WIRE 2416 4160 2400 4160
WIRE 2528 4176 2480 4176
WIRE 2592 4176 2528 4176
WIRE 2720 4176 2720 4032
WIRE 2720 4176 2672 4176
WIRE 2784 4176 2720 4176
WIRE 2880 4176 2784 4176
WIRE 3024 4176 2960 4176
WIRE 3120 4176 3024 4176
WIRE 3200 4176 3120 4176
WIRE 3360 4176 3264 4176
WIRE 1328 4192 1328 864
WIRE 1328 4192 1312 4192
WIRE 1424 4192 1376 4192
WIRE 1584 4192 1504 4192
WIRE 1728 4192 1664 4192
WIRE 1824 4192 1728 4192
WIRE 2016 4192 1824 4192
WIRE 2176 4192 2096 4192
WIRE 2416 4192 2176 4192
WIRE 3024 4208 3024 4176
WIRE 3120 4208 3120 4176
WIRE 1728 4224 1728 4192
WIRE 1824 4224 1824 4192
WIRE 2176 4224 2176 4192
WIRE 3200 4272 3200 4176
WIRE 3120 4336 3120 4288
WIRE 1824 4352 1824 4304
WIRE 5088 4688 4256 4688
WIRE 4352 4752 4272 4752
WIRE 4496 4752 4416 4752
WIRE 6160 4784 5952 4784
WIRE 6208 4784 6160 4784
WIRE 4864 4800 4800 4800
WIRE 6208 4800 6208 4784
WIRE 4800 4816 4800 4800
WIRE 4864 4816 4864 4800
WIRE 5952 4832 5952 4784
WIRE 6160 4832 6160 4784
WIRE 4256 4864 4256 4688
WIRE 4272 4864 4272 4752
WIRE 4272 4864 4256 4864
WIRE 4336 4864 4272 4864
WIRE 4496 4864 4496 4752
WIRE 4496 4864 4416 4864
WIRE 4608 4864 4496 4864
WIRE 5088 4864 5088 4688
WIRE 5088 4864 5056 4864
WIRE 4608 4912 4608 4864
WIRE 4752 4912 4608 4912
WIRE 4336 4928 4160 4928
WIRE 4608 4928 4608 4912
WIRE 4608 4928 4416 4928
WIRE 5184 4928 5056 4928
WIRE 5360 4928 5264 4928
WIRE 5536 4928 5360 4928
WIRE 5840 4928 5536 4928
WIRE 5952 4928 5952 4896
WIRE 5952 4928 5920 4928
WIRE 6016 4928 5952 4928
WIRE 6160 4928 6160 4896
WIRE 6160 4928 6096 4928
WIRE 6304 4928 6160 4928
WIRE 4656 4960 4640 4960
WIRE 4752 4960 4720 4960
WIRE 4336 4992 4176 4992
WIRE 4608 4992 4416 4992
WIRE 5184 4992 5056 4992
WIRE 5360 4992 5264 4992
WIRE 5536 4992 5360 4992
WIRE 6160 4992 5952 4992
WIRE 6208 4992 6160 4992
WIRE 4608 5008 4608 4992
WIRE 4752 5008 4608 5008
WIRE 6208 5008 6208 4992
WIRE 5952 5040 5952 4992
WIRE 6160 5040 6160 4992
WIRE 4272 5056 4256 5056
WIRE 4336 5056 4272 5056
WIRE 4496 5056 4416 5056
WIRE 4608 5056 4608 5008
WIRE 4608 5056 4496 5056
WIRE 5088 5056 5056 5056
WIRE 4864 5136 4864 5104
WIRE 5536 5136 5536 4992
WIRE 5840 5136 5536 5136
WIRE 5952 5136 5952 5104
WIRE 5952 5136 5920 5136
WIRE 6016 5136 5952 5136
WIRE 6160 5136 6160 5104
WIRE 6160 5136 6096 5136
WIRE 6304 5136 6160 5136
WIRE 3296 5152 3232 5152
WIRE 3776 5152 3376 5152
WIRE 4160 5152 4160 4928
WIRE 4160 5152 3840 5152
WIRE 4272 5152 4272 5056
WIRE 4352 5152 4272 5152
WIRE 4496 5152 4496 5056
WIRE 4496 5152 4416 5152
WIRE 3232 5184 3232 5152
WIRE 3920 5184 3856 5184
WIRE 4176 5184 4176 4992
WIRE 4176 5184 3920 5184
WIRE 4256 5200 4256 5056
WIRE 5088 5200 5088 5056
WIRE 5088 5200 4256 5200
WIRE 2784 5248 2784 4176
WIRE 2896 5248 2784 5248
WIRE 3040 5248 2976 5248
WIRE 3136 5248 3040 5248
WIRE 3264 5248 3136 5248
WIRE 3776 5248 3264 5248
WIRE 3856 5248 3856 5184
WIRE 3856 5248 3840 5248
WIRE 4720 5264 4720 4960
WIRE 3040 5280 3040 5248
WIRE 3136 5280 3136 5248
WIRE 4528 5280 4400 5280
WIRE 4720 5312 4720 5264
WIRE 3680 5360 3584 5360
WIRE 3728 5360 3680 5360
WIRE 4048 5360 4016 5360
WIRE 4400 5360 4400 5280
WIRE 4432 5360 4400 5360
WIRE 4528 5376 4528 5280
WIRE 4528 5376 4496 5376
WIRE 4048 5392 4048 5360
WIRE 4128 5392 4048 5392
WIRE 4176 5392 4128 5392
WIRE 4320 5392 4256 5392
WIRE 4384 5392 4320 5392
WIRE 4432 5392 4384 5392
WIRE 4528 5392 4528 5376
WIRE 4672 5392 4528 5392
WIRE 4720 5392 4672 5392
WIRE 5056 5392 4928 5392
WIRE 5232 5392 5136 5392
WIRE 3136 5408 3136 5360
WIRE 4128 5408 4128 5392
WIRE 3680 5424 3680 5360
WIRE 3728 5424 3680 5424
WIRE 4048 5424 4048 5392
WIRE 4048 5424 4016 5424
WIRE 3584 5456 3584 5424
WIRE 3872 5488 3872 5456
WIRE 4128 5488 4128 5472
WIRE 4320 5488 4320 5456
WIRE 4672 5488 4672 5456
WIRE 4720 5536 4720 5472
WIRE 4384 5616 4384 5392
WIRE 4928 5616 4928 5392
WIRE 4928 5616 4384 5616
FLAG 336 1072 0
FLAG 288 1056 0
FLAG 944 704 VDD_3
FLAG 1184 992 0
FLAG 576 1072 0
FLAG 528 1056 0
FLAG 816 1072 0
FLAG 768 1056 0
FLAG -304 912 0
FLAG -304 832 VDD_3
FLAG 1232 864 OUT_PRE
FLAG -800 1552 0
FLAG -800 1472 CON_S1
FLAG 288 1008 CON_S1
FLAG -800 1760 0
FLAG -800 1680 CON_S2
FLAG 528 1008 CON_S2
FLAG -800 1968 0
FLAG -800 1888 CON_S3
FLAG 768 1008 CON_S3
FLAG -416 912 0
FLAG -416 832 VSS
FLAG -528 912 0
FLAG -528 832 VDD
FLAG -528 1088 AC_TEST
FLAG -528 1168 0
FLAG 432 864 AC_TEST
FLAG 1008 1232 0
FLAG 1056 928 0
FLAG 1824 4416 0
FLAG 2176 4288 0
FLAG 1728 4304 0
FLAG 2448 4144 VDD_10
FLAG 2448 4208 VSS_10
FLAG 3120 4400 0
FLAG 3024 4272 0
FLAG 3360 4256 0
FLAG 2256 4096 0
FLAG 2528 4240 0
FLAG 3920 5184 Vx_ac
FLAG 4640 4960 0
FLAG 4864 5136 0
FLAG 5536 4928 Vx_adc_inp
FLAG 5536 4992 Vx_adc_inn
FLAG 4864 4800 VDD
FLAG 3584 5456 0
FLAG 3584 5360 VDD
FLAG 5232 5392 Vcm_adc
FLAG 3872 5488 0
FLAG 4128 5488 0
FLAG 4320 5488 0
FLAG 4464 5408 0
FLAG 4464 5344 VDD
FLAG 4672 5488 0
FLAG 5360 5056 0
FLAG 5360 4864 0
FLAG 4864 5264 0
FLAG 4864 5536 0
FLAG 3232 5184 0
FLAG 3136 5472 0
FLAG 3040 5344 0
FLAG 6208 4800 0
FLAG 6304 4928 Vx_adc_samp_p
FLAG 6208 5008 0
FLAG 3200 4352 0
FLAG 3264 5328 0
FLAG 6304 5136 Vx_adc_samp_n
FLAG 2576 -208 0
FLAG 2528 -224 0
FLAG 3184 -576 VDD_3
FLAG 3696 -288 0
FLAG 2816 -208 0
FLAG 2768 -224 0
FLAG 3056 -208 0
FLAG 3008 -224 0
FLAG 2528 -272 CON_S1
FLAG 2768 -272 CON_S2
FLAG 3008 -272 CON_S3
FLAG 3248 -176 0
FLAG 3440 -288 0
FLAG 3744 -416 dwave_out
FLAG 2576 320 0
FLAG 2528 304 0
FLAG 3184 -48 VDD_3
FLAG 3696 240 0
FLAG 2816 320 0
FLAG 2768 304 0
FLAG 3056 320 0
FLAG 3008 304 0
FLAG 2528 256 CON_S1
FLAG 2768 256 CON_S2
FLAG 3008 256 CON_S3
FLAG 3248 352 0
FLAG 3440 240 0
FLAG 3744 112 dwave_out_a
FLAG 4400 928 0
FLAG 4752 800 0
FLAG 4304 816 0
FLAG 2512 912 0
FLAG 2464 896 0
FLAG 3120 544 VDD_3
FLAG 3632 832 0
FLAG 2752 912 0
FLAG 2704 896 0
FLAG 2992 912 0
FLAG 2944 896 0
FLAG 2464 848 CON_S1
FLAG 2704 848 CON_S2
FLAG 2944 848 CON_S3
FLAG 3184 944 0
FLAG 3376 832 0
FLAG 4752 704 opa_in
FLAG 4400 1568 0
FLAG 4752 1440 0
FLAG 4304 1456 0
FLAG 2512 1552 0
FLAG 2464 1536 0
FLAG 3120 1184 VDD_3
FLAG 3632 1472 0
FLAG 2752 1552 0
FLAG 2704 1536 0
FLAG 2992 1552 0
FLAG 2944 1536 0
FLAG 2464 1488 CON_S1
FLAG 2704 1488 CON_S2
FLAG 2944 1488 CON_S3
FLAG 3184 1584 0
FLAG 3376 1472 0
FLAG 4752 1344 opa_in_a
FLAG 5088 2064 VDD_10
FLAG 5088 2128 VSS_10
FLAG 5760 2320 0
FLAG 5664 2192 0
FLAG 5456 2096 buf_out
FLAG 6000 2176 0
FLAG 4896 2016 0
FLAG 5168 2160 0
FLAG 4384 2336 0
FLAG 4736 2208 0
FLAG 4288 2224 0
FLAG 2496 2320 0
FLAG 2448 2304 0
FLAG 3104 1952 VDD_3
FLAG 3616 2240 0
FLAG 2736 2320 0
FLAG 2688 2304 0
FLAG 2976 2320 0
FLAG 2928 2304 0
FLAG 2448 2256 CON_S1
FLAG 2688 2256 CON_S2
FLAG 2928 2256 CON_S3
FLAG 3168 2352 0
FLAG 3360 2240 0
FLAG 5088 2848 VDD_10
FLAG 5088 2912 VSS_10
FLAG 5760 3104 0
FLAG 5664 2976 0
FLAG 5456 2880 buf_out_a
FLAG 6000 2960 0
FLAG 4896 2800 0
FLAG 5168 2944 0
FLAG 4384 3120 0
FLAG 4736 2992 0
FLAG 4288 3008 0
FLAG 2496 3104 0
FLAG 2448 3088 0
FLAG 3104 2736 VDD_3
FLAG 3616 3024 0
FLAG 2736 3104 0
FLAG 2688 3088 0
FLAG 2976 3104 0
FLAG 2928 3088 0
FLAG 2448 3040 CON_S1
FLAG 2688 3040 CON_S2
FLAG 2928 3040 CON_S3
FLAG 3168 3136 0
FLAG 3360 3024 0
FLAG 5360 2944 0
FLAG -432 720 0
FLAG -432 640 VSS_10
FLAG -544 720 0
FLAG -544 640 VDD_10
FLAG 5824 2096 diff_in
FLAG 5792 2880 diff_in_a
FLAG 5616 3088 0
DATAFLAG 4080 5392 ""
DATAFLAG 3728 5248 ""
DATAFLAG 4016 5184 ""
DATAFLAG 5456 4928 ""
DATAFLAG 5456 4992 ""
DATAFLAG 5184 5392 ""
DATAFLAG 2800 4176 ""
DATAFLAG 1904 4192 ""
DATAFLAG 1328 1392 ""
DATAFLAG 4480 704 ""
DATAFLAG 4480 1344 ""
DATAFLAG 5360 1952 ""
DATAFLAG 4464 2112 ""
DATAFLAG 5360 2736 ""
DATAFLAG 4464 2896 ""
SYMBOL sw 336 1088 M180
SYMATTR InstName S1
SYMBOL res 928 688 R0
SYMATTR InstName R1
SYMATTR Value {R_UP}
SYMBOL res 320 896 R0
SYMATTR InstName R2
SYMATTR Value {R_S1}
SYMBOL res 1168 880 R0
SYMATTR InstName R3
SYMATTR Value 24.9k
SYMBOL sw 576 1088 M180
SYMATTR InstName S2
SYMBOL res 560 896 R0
SYMATTR InstName R4
SYMATTR Value {R_S2}
SYMBOL sw 816 1088 M180
SYMATTR InstName S3
SYMBOL res 800 896 R0
SYMATTR InstName R5
SYMATTR Value {R_S3}
SYMBOL voltage -304 816 R0
SYMATTR InstName V3
SYMATTR Value 3.3
SYMBOL voltage -800 1456 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V4
SYMATTR Value PULSE(0 1 {3*PERIOD_8F} 1p 1p {2*PERIOD_8F} {PERIOD_F} 0)
SYMBOL voltage -800 1664 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V5
SYMATTR Value PULSE(0 1 {2*PERIOD_8F} 1p 1p {4*PERIOD_8F} {PERIOD_F} 0)
SYMBOL voltage -800 1872 R0
WINDOW 123 0 0 Left 2
WINDOW 39 0 0 Left 2
SYMATTR InstName V6
SYMATTR Value PULSE(0 1 {PERIOD_8F} 1p 1p {6*PERIOD_8F} {PERIOD_F} 0)
SYMBOL voltage -416 816 R0
SYMATTR InstName V7
SYMATTR Value -5
SYMBOL voltage -528 816 R0
SYMATTR InstName V8
SYMATTR Value 5
SYMBOL voltage -528 1072 R0
WINDOW 123 24 152 Left 2
WINDOW 39 24 124 Left 2
SYMATTR Value2 AC 1u
SYMATTR SpiceLine Rser=250k
SYMATTR InstName V1
SYMATTR Value 0
SYMBOL cap 1104 880 R270
WINDOW 0 32 32 VTop 2
WINDOW 3 0 32 VBottom 2
SYMATTR InstName C22
SYMATTR Value 47n
SYMBOL res 992 1136 R0
SYMATTR InstName R6
SYMATTR Value {R_DN}
SYMBOL cap 1040 864 R0
SYMATTR InstName C21
SYMATTR Value 10p
SYMBOL cap 1376 4176 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C23
SYMATTR Value 47n
SYMBOL res 1680 4176 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R9
SYMATTR Value 0.22
SYMBOL res 2112 4176 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R10
SYMATTR Value 100
SYMBOL cap 2160 4224 R0
SYMATTR InstName C24
SYMATTR Value 10p
SYMBOL res 1712 4208 R0
SYMATTR InstName R11
SYMATTR Value 24.9k
SYMBOL res 1808 4208 R0
SYMATTR InstName R12
SYMATTR Value 0.22
SYMBOL cap 1808 4352 R0
SYMATTR InstName C25
SYMATTR Value 56p
SYMBOL res 2688 4160 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R13
SYMATTR Value 0.22
SYMBOL res 2608 4016 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R14
SYMATTR Value 0.22
SYMBOL cap 2608 4096 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C26
SYMATTR Value 180p
SYMBOL ths4631_test 2448 4112 R0
SYMATTR InstName U3
SYMBOL res 2976 4160 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R25
SYMATTR Value 100
SYMBOL res 3104 4192 R0
SYMATTR InstName R27
SYMATTR Value 0.22
SYMBOL cap 3104 4336 R0
SYMATTR InstName C27
SYMATTR Value 56p
SYMBOL cap 3008 4208 R0
SYMATTR InstName C28
SYMATTR Value 10p
SYMBOL cap 3264 4160 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C29
SYMATTR Value 470n
SYMBOL res 3344 4160 R0
SYMATTR InstName R28
SYMATTR Value 6000
SYMBOL cap 2240 4032 R0
SYMATTR InstName C30
SYMATTR Value 33p
SYMBOL cap 2512 4176 R0
SYMATTR InstName C31
SYMATTR Value 0p
SYMBOL cap 3840 5136 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C1
SYMATTR Value 470n
SYMBOL cap 4720 4944 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C2
SYMATTR Value .1�
SYMBOL cap 3840 5232 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C3
SYMATTR Value 470n
SYMBOL res 4704 5296 R0
SYMATTR InstName R7
SYMATTR Value 0.22
SYMBOL res 4704 5376 R0
SYMATTR InstName R8
SYMATTR Value 0.22
SYMBOL cap 3568 5360 R0
SYMATTR InstName C4
SYMATTR Value 1�
SYMBOL res 4272 5376 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R15
SYMATTR Value 0.22
SYMBOL cap 4112 5408 R0
SYMATTR InstName C5
SYMATTR Value 3.3�
SYMBOL LTC6655-2.048 3872 5392 R0
SYMATTR InstName U1
SYMBOL Opamps\\LTC6404-4 4880 4960 R0
SYMATTR InstName U2
SYMBOL cap 4336 5456 R180
WINDOW 0 24 56 Left 2
WINDOW 3 24 8 Left 2
SYMATTR InstName C7
SYMATTR Value 3.3�
SYMBOL cap 4416 4736 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C8
SYMATTR Value 0.1p
SYMBOL cap 4416 5136 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C9
SYMATTR Value 0.1p
SYMBOL Opamps\\LT6202 4464 5376 R0
SYMATTR InstName U5
SYMBOL cap 4688 5456 R180
WINDOW 0 24 56 Left 2
WINDOW 3 24 8 Left 2
SYMATTR InstName C10
SYMATTR Value 2.2�
SYMBOL res 5152 5376 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R17
SYMATTR Value 0.22
SYMBOL res 5280 4912 R90
WINDOW 0 10 100 VBottom 2
WINDOW 3 20 8 VTop 2
SYMATTR InstName R18
SYMATTR Value 10
SYMBOL res 5280 4976 R90
WINDOW 0 14 101 VBottom 2
WINDOW 3 24 7 VTop 2
SYMATTR InstName R19
SYMATTR Value 10
SYMBOL cap 5344 4864 R0
SYMATTR InstName C11
SYMATTR Value 0p
SYMBOL cap 5344 4992 R0
SYMATTR InstName C12
SYMATTR Value 0p
SYMBOL res 4816 5248 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R21
SYMATTR Value 0.22
SYMBOL cap 4864 5248 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C13
SYMATTR Value 2.2�
SYMBOL res 4816 5520 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R22
SYMATTR Value 0.22
SYMBOL cap 4864 5520 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C14
SYMATTR Value 2.2�
SYMBOL res 3392 5136 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R16
SYMATTR Value 100
SYMBOL res 2992 5232 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R20
SYMATTR Value 100
SYMBOL res 3120 5264 R0
SYMATTR InstName R23
SYMATTR Value 0.22
SYMBOL cap 3120 5408 R0
SYMATTR InstName C6
SYMATTR Value 56p
SYMBOL cap 3024 5280 R0
SYMATTR InstName C15
SYMATTR Value 10p
SYMBOL res 5936 4912 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R24
SYMATTR Value 10
SYMBOL cap 5936 4832 R0
SYMATTR InstName C16
SYMATTR Value 2p
SYMBOL res 6112 4912 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R26
SYMATTR Value 28
SYMBOL cap 6144 4832 R0
SYMATTR InstName C17
SYMATTR Value 18p
SYMBOL res 4432 4848 R90
WINDOW 0 12 118 VBottom 2
WINDOW 3 28 6 VTop 2
SYMATTR InstName R29
SYMATTR Value 5000
SYMBOL res 4432 4912 R90
WINDOW 0 12 117 VBottom 2
WINDOW 3 23 6 VTop 2
SYMATTR InstName R30
SYMATTR Value 1000
SYMBOL res 4432 4976 R90
WINDOW 0 15 114 VBottom 2
WINDOW 3 32 9 VTop 2
SYMATTR InstName R31
SYMATTR Value 1000
SYMBOL res 4432 5040 R90
WINDOW 0 10 117 VBottom 2
WINDOW 3 29 6 VTop 2
SYMATTR InstName R32
SYMATTR Value 5000
SYMBOL res 5936 5120 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R33
SYMATTR Value 10
SYMBOL cap 5936 5040 R0
SYMATTR InstName C18
SYMATTR Value 2p
SYMBOL res 6112 5120 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R34
SYMATTR Value 28
SYMBOL cap 6144 5040 R0
SYMATTR InstName C19
SYMATTR Value 18p
SYMBOL res 3184 4256 R0
SYMATTR InstName R35
SYMATTR Value 124
SYMBOL res 3248 5232 R0
SYMATTR InstName R36
SYMATTR Value 124
SYMBOL sw 2576 -192 M180
SYMATTR InstName S4
SYMBOL res 3168 -592 R0
SYMATTR InstName R37
SYMATTR Value {R_UP}
SYMBOL res 2560 -384 R0
SYMATTR InstName R38
SYMATTR Value {R_S1}
SYMBOL res 3680 -400 R0
SYMATTR InstName R39
SYMATTR Value 24.9k
SYMBOL sw 2816 -192 M180
SYMATTR InstName S5
SYMBOL res 2800 -384 R0
SYMATTR InstName R40
SYMATTR Value {R_S2}
SYMBOL sw 3056 -192 M180
SYMATTR InstName S6
SYMBOL res 3040 -384 R0
SYMATTR InstName R41
SYMATTR Value {R_S3}
SYMBOL cap 3568 -400 R270
WINDOW 0 32 32 VTop 2
WINDOW 3 0 32 VBottom 2
SYMATTR InstName C20
SYMATTR Value 47n
SYMBOL res 3232 -272 R0
SYMATTR InstName R42
SYMATTR Value {R_DN}
SYMBOL cap 3424 -352 R0
SYMATTR InstName C32
SYMATTR Value 10p
SYMBOL sw 2576 336 M180
SYMATTR InstName S7
SYMBOL res 3168 -64 R0
SYMATTR InstName R43
SYMATTR Value {R_UP}
SYMBOL res 2560 144 R0
SYMATTR InstName R44
SYMATTR Value {R_S1}
SYMBOL res 3680 128 R0
SYMATTR InstName R45
SYMATTR Value 24.9k
SYMBOL sw 2816 336 M180
SYMATTR InstName S8
SYMBOL res 2800 144 R0
SYMATTR InstName R46
SYMATTR Value {R_S2}
SYMBOL sw 3056 336 M180
SYMATTR InstName S9
SYMBOL res 3040 144 R0
SYMATTR InstName R47
SYMATTR Value {R_S3}
SYMBOL cap 3568 128 R270
WINDOW 0 32 32 VTop 2
WINDOW 3 0 32 VBottom 2
SYMATTR InstName C33
SYMATTR Value 47n
SYMBOL res 3232 256 R0
SYMATTR InstName R48
SYMATTR Value {R_DN}
SYMBOL cap 3424 176 R0
SYMATTR InstName C34
SYMATTR Value 43p
SYMBOL cap 3936 688 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C35
SYMATTR Value 47n
SYMBOL res 4240 688 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R49
SYMATTR Value 0.22
SYMBOL res 4688 688 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R50
SYMATTR Value 100
SYMBOL cap 4736 736 R0
SYMATTR InstName C36
SYMATTR Value 10p
SYMBOL res 4288 720 R0
SYMATTR InstName R51
SYMATTR Value 24.9k
SYMBOL res 4384 720 R0
SYMATTR InstName R52
SYMATTR Value 0.22
SYMBOL cap 4384 864 R0
SYMATTR InstName C37
SYMATTR Value 56p
SYMBOL sw 2512 928 M180
SYMATTR InstName S10
SYMBOL res 3104 528 R0
SYMATTR InstName R53
SYMATTR Value {R_UP}
SYMBOL res 2496 736 R0
SYMATTR InstName R54
SYMATTR Value {R_S1}
SYMBOL res 3616 720 R0
SYMATTR InstName R55
SYMATTR Value 24.9k
SYMBOL sw 2752 928 M180
SYMATTR InstName S11
SYMBOL res 2736 736 R0
SYMATTR InstName R56
SYMATTR Value {R_S2}
SYMBOL sw 2992 928 M180
SYMATTR InstName S12
SYMBOL res 2976 736 R0
SYMATTR InstName R57
SYMATTR Value {R_S3}
SYMBOL cap 3504 720 R270
WINDOW 0 32 32 VTop 2
WINDOW 3 0 32 VBottom 2
SYMATTR InstName C38
SYMATTR Value 47n
SYMBOL res 3168 848 R0
SYMATTR InstName R58
SYMATTR Value {R_DN}
SYMBOL cap 3360 768 R0
SYMATTR InstName C39
SYMATTR Value 10p
SYMBOL ind 1408 4208 R270
WINDOW 0 32 56 VTop 2
WINDOW 3 5 56 VBottom 2
SYMATTR InstName L1
SYMATTR Value 6.4n
SYMATTR SpiceLine Ipk=0.4
SYMBOL ind 3968 1360 R270
WINDOW 0 32 56 VTop 2
WINDOW 3 5 56 VBottom 2
SYMATTR InstName L2
SYMATTR Value 64n
SYMBOL cap 3936 1328 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C40
SYMATTR Value 47n
SYMBOL res 4224 1328 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R59
SYMATTR Value 0.22
SYMBOL res 4688 1328 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R60
SYMATTR Value 100
SYMBOL cap 4736 1376 R0
SYMATTR InstName C41
SYMATTR Value 10p
SYMBOL res 4288 1360 R0
SYMATTR InstName R61
SYMATTR Value 24.9k
SYMBOL res 4384 1360 R0
SYMATTR InstName R62
SYMATTR Value 0.22
SYMBOL cap 4384 1504 R0
SYMATTR InstName C42
SYMATTR Value 110p
SYMBOL sw 2512 1568 M180
SYMATTR InstName S13
SYMBOL res 3104 1168 R0
SYMATTR InstName R63
SYMATTR Value {R_UP}
SYMBOL res 2496 1376 R0
SYMATTR InstName R64
SYMATTR Value {R_S1}
SYMBOL res 3616 1360 R0
SYMATTR InstName R65
SYMATTR Value 24.9k
SYMBOL sw 2752 1568 M180
SYMATTR InstName S14
SYMBOL res 2736 1376 R0
SYMATTR InstName R66
SYMATTR Value {R_S2}
SYMBOL sw 2992 1568 M180
SYMATTR InstName S15
SYMBOL res 2976 1376 R0
SYMATTR InstName R67
SYMATTR Value {R_S3}
SYMBOL cap 3504 1360 R270
WINDOW 0 32 32 VTop 2
WINDOW 3 0 32 VBottom 2
SYMATTR InstName C43
SYMATTR Value 47n
SYMBOL res 3168 1488 R0
SYMATTR InstName R68
SYMATTR Value {R_DN}
SYMBOL cap 3360 1408 R0
SYMATTR InstName C44
SYMATTR Value 43p
SYMBOL ind 3968 720 R270
WINDOW 0 32 56 VTop 2
WINDOW 3 5 56 VBottom 2
SYMATTR InstName L3
SYMATTR Value 64n
SYMBOL res 5328 2080 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R69
SYMATTR Value 0.22
SYMBOL res 5248 1936 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R70
SYMATTR Value 0.22
SYMBOL cap 5248 2016 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C45
SYMATTR Value 0p
SYMBOL ths4631_test 5088 2032 R0
SYMATTR InstName U4
SYMBOL res 5616 2080 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R71
SYMATTR Value 100
SYMBOL res 5744 2112 R0
SYMATTR InstName R72
SYMATTR Value 0.22
SYMBOL cap 5744 2256 R0
SYMATTR InstName C46
SYMATTR Value 0p
SYMBOL cap 5648 2128 R0
SYMATTR InstName C47
SYMATTR Value 0p
SYMBOL cap 5904 2080 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C48
SYMATTR Value {470n+47n}
SYMBOL res 5984 2080 R0
SYMATTR InstName R73
SYMATTR Value 6000
SYMBOL cap 4880 1952 R0
SYMATTR InstName C49
SYMATTR Value 100p
SYMBOL cap 5152 2096 R0
SYMATTR InstName C50
SYMATTR Value 100p
SYMBOL ind 3952 2128 R270
WINDOW 0 32 56 VTop 2
WINDOW 3 5 56 VBottom 2
SYMATTR InstName L4
SYMATTR Value 64n
SYMBOL cap 3920 2096 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C51
SYMATTR Value 47n
SYMBOL res 4208 2096 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R75
SYMATTR Value 0.22
SYMBOL res 4672 2096 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R76
SYMATTR Value 100
SYMBOL cap 4720 2144 R0
SYMATTR InstName C52
SYMATTR Value 0p
SYMBOL res 4272 2128 R0
SYMATTR InstName R77
SYMATTR Value 24.9k
SYMBOL res 4368 2128 R0
SYMATTR InstName R78
SYMATTR Value 0.22
SYMBOL cap 4368 2272 R0
SYMATTR InstName C53
SYMATTR Value 0p
SYMBOL sw 2496 2336 M180
SYMATTR InstName S16
SYMBOL res 3088 1936 R0
SYMATTR InstName R79
SYMATTR Value {R_UP}
SYMBOL res 2480 2144 R0
SYMATTR InstName R80
SYMATTR Value {R_S1}
SYMBOL res 3600 2128 R0
SYMATTR InstName R81
SYMATTR Value 24.9k
SYMBOL sw 2736 2336 M180
SYMATTR InstName S17
SYMBOL res 2720 2144 R0
SYMATTR InstName R82
SYMATTR Value {R_S2}
SYMBOL sw 2976 2336 M180
SYMATTR InstName S18
SYMBOL res 2960 2144 R0
SYMATTR InstName R83
SYMATTR Value {R_S3}
SYMBOL cap 3488 2128 R270
WINDOW 0 32 32 VTop 2
WINDOW 3 0 32 VBottom 2
SYMATTR InstName C54
SYMATTR Value 47n
SYMBOL res 3152 2256 R0
SYMATTR InstName R84
SYMATTR Value {R_DN}
SYMBOL cap 3344 2176 R0
SYMATTR InstName C55
SYMATTR Value 43p
SYMBOL res 5328 2864 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R85
SYMATTR Value 0.22
SYMBOL res 5248 2720 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R86
SYMATTR Value 0.22
SYMBOL cap 5248 2800 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C56
SYMATTR Value 0p
SYMBOL ths4631_test 5088 2816 R0
SYMATTR InstName U6
SYMBOL res 5616 2864 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R87
SYMATTR Value 100
SYMBOL res 5744 2896 R0
SYMATTR InstName R88
SYMATTR Value 0.22
SYMBOL cap 5744 3040 R0
SYMATTR InstName C57
SYMATTR Value 10p
SYMBOL cap 5648 2912 R0
SYMATTR InstName C58
SYMATTR Value 0p
SYMBOL cap 5904 2864 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C59
SYMATTR Value {470n+47n}
SYMBOL res 5984 2864 R0
SYMATTR InstName R89
SYMATTR Value 6000
SYMBOL cap 4880 2736 R0
SYMATTR InstName C60
SYMATTR Value 100p
SYMBOL cap 5152 2880 R0
SYMATTR InstName C61
SYMATTR Value 0p
SYMBOL ind 3952 2912 R270
WINDOW 0 32 56 VTop 2
WINDOW 3 5 56 VBottom 2
SYMATTR InstName L5
SYMATTR Value 64n
SYMBOL cap 3920 2880 R90
WINDOW 0 0 32 VBottom 2
WINDOW 3 32 32 VTop 2
SYMATTR InstName C62
SYMATTR Value 47n
SYMBOL res 4208 2880 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R91
SYMATTR Value 0.22
SYMBOL res 4672 2880 R90
WINDOW 0 0 56 VBottom 2
WINDOW 3 32 56 VTop 2
SYMATTR InstName R92
SYMATTR Value 100
SYMBOL cap 4720 2928 R0
SYMATTR InstName C63
SYMATTR Value 10p
SYMBOL res 4272 2912 R0
SYMATTR InstName R93
SYMATTR Value 24.9k
SYMBOL res 4368 2912 R0
SYMATTR InstName R94
SYMATTR Value 0.22
SYMBOL cap 4368 3056 R0
SYMATTR InstName C64
SYMATTR Value 56p
SYMBOL sw 2496 3120 M180
SYMATTR InstName S19
SYMBOL res 3088 2720 R0
SYMATTR InstName R95
SYMATTR Value {R_UP}
SYMBOL res 2480 2928 R0
SYMATTR InstName R96
SYMATTR Value {R_S1}
SYMBOL res 3600 2912 R0
SYMATTR InstName R97
SYMATTR Value 24.9k
SYMBOL sw 2736 3120 M180
SYMATTR InstName S20
SYMBOL res 2720 2928 R0
SYMATTR InstName R98
SYMATTR Value {R_S2}
SYMBOL sw 2976 3120 M180
SYMATTR InstName S21
SYMBOL res 2960 2928 R0
SYMATTR InstName R99
SYMATTR Value {R_S3}
SYMBOL cap 3488 2912 R270
WINDOW 0 32 32 VTop 2
WINDOW 3 0 32 VBottom 2
SYMATTR InstName C65
SYMATTR Value 47n
SYMBOL res 3152 3040 R0
SYMATTR InstName R100
SYMATTR Value {R_DN}
SYMBOL cap 3344 2960 R0
SYMATTR InstName C66
SYMATTR Value 43p
SYMBOL cap 5344 2880 R0
SYMATTR InstName C67
SYMATTR Value 0p
SYMBOL voltage -432 624 R0
SYMATTR InstName V2
SYMATTR Value -10
SYMBOL voltage -544 624 R0
WINDOW 123 0 0 Left 0
WINDOW 39 0 0 Left 0
SYMATTR InstName V9
SYMATTR Value 10
SYMBOL res 5600 2992 R0
SYMATTR InstName R74
SYMATTR Value 124
TEXT -808 1136 Left 2 !;.ac dec 5 10 100Meg
TEXT -704 1400 Left 2 !.model SW SW(Ron=1 Roff=1Meg Vt=.5 Vh=-.4)
TEXT -136 320 Left 2 !;.param FREQ_TEST = {100k}\n;.param FREQ_TEST = {1Meg}
TEXT 520 -104 Left 2 ;1 / (2 * pi * (100 ohm) * (47 nF)) = 33.8627538 kilohertz\n1 / (2 * pi * (24.9 kiloohm) * (470 nF)) = 13.5994995 hertz\n1 / (2 * pi * (24.9 kiloohm) * (47 nF)) = 135.994995 hertz\n1 / (2 * pi * (24.9 kiloohm) * (1 pF)) = 6.39176478 megahertz\n1 / (2 * pi * (24.9 kiloohm) * (0.47 pF)) = 13.5994995 megahertz\n1 / (2 * pi * (24.9 ohm) * (100 pF)) = 63.9176478 megahertz\n1 / (2 * pi * (24.9 ohm) * (470 pF)) = 13.5994995 megahertz\n1 / (2 * pi * (24.9 ohm) * (4.7 nF)) = 1.35994995 megahertz\n1 / (2 * pi * (24.9 ohm) * (47 nF)) = 135.994995 kilohertz\n1 / (2 * pi * (24.9 ohm) * (470 nF)) = 13.5994995 kilohertz\n1 / (2 * pi * (24.9 ohm) * (100 nF)) = 63.9176478 kilohertz\n1 / (2 * pi * (24.9 ohm) * (47 uF)) = 135.994995 hertz\n1 / (2 * pi * (500 ohm) * (1000 nF)) = 318.309886 hertz\n1 / (2 * pi * (500 ohm) * (100 nF)) = 3.18309886 kilohertz
TEXT -1152 1672 Left 2 ;*8f-4level\n--      --\n  -    -\n   -  -\n    --
TEXT -824 1280 Left 2 !.param PERIOD_F = {1/{FREQ_TEST}}\n.param PERIOD_8F = {1/{FREQ_TEST}/8}
TEXT 2480 -808 Left 2 !;.param R_UP=499 R_S1=866 R_S2=1070 R_S3=2320 R_DN=2000\n.param R_UP={124/3} R_S1={215/3} R_S2={267/3} R_S3={576/3} R_DN={499/3}\n;.param R_UP={124/2} R_S1={215/2} R_S2={267/2} R_S3={576/2} R_DN={499/2}\n;.param R_UP={124/1} R_S1={215/1} R_S2={267/1} R_S3={576/1} R_DN={499/1}
TEXT -136 656 Left 2 !.tran {PERIOD_F*20}\n;.tran 0.5m
TEXT -136 400 Left 2 !;.step param FREQ_TEST list 10k 100k 1Meg 5Meg\n;.step param FREQ_TEST list 1k 10k 100k 1Meg 5Meg\n;.step param FREQ_TEST list 1Meg 5Meg 10Meg\n.step param FREQ_TEST list 100k 1Meg 5Meg 10Meg\n;.step param FREQ_TEST list 10k 100k 1Meg 5Meg\n;.step param FREQ_TEST list 10k 20k 50k 100k 200k 500k 1Meg 5Meg\n;.step param FREQ_TEST list 10k 100k 1Meg 10Meg
TEXT 752 416 Left 2 ;=== R net ===\n499\n866\n1070\n2320\n2000\n...\n124/3\n215/3\n267/3\n576/3\n499/3
TEXT 992 968 Left 2 ;10pF vs 22pF vs 56pF
TEXT 2176 4344 Left 2 ;10p vs 0p
TEXT 1816 4464 Left 2 ;2.2p vs 10p vs 56p vs 1n
TEXT 2016 4080 Left 2 ;cause of overshoot
TEXT 2744 4080 Left 2 ;10p vs 180p vs 270p vs 1000p
TEXT 3432 4752 Left 2 ;* filter time constant\n1 / (2 * pi * (24.9 ohm) * (1000 nF)) = 6.39176478 kilohertz\n1 / (2 * pi * (24.9 ohm) * (330 pF)) = 19.4 megahertz\n1 / (2 * pi * (24.9 ohm) * ((330/2) pF)) = 38.7 megahertz\n1 / (2 * pi * (300 ohm) * (47 nF)) = 11.2875846 kilohertz
TEXT 1560 4096 Left 2 ;0.22 vs 24.9k
TEXT 2504 3952 Left 2 ;0.22 vs 500
TEXT 3360 -240 Left 2 ;10pF vs 22pF vs 33pF vs 43pF vs 56pF
TEXT 4752 856 Left 2 ;10p vs 0p
TEXT 4392 976 Left 2 ;2.2p vs 10p vs 56p vs 1n
TEXT 3296 880 Left 2 ;10pF vs 22pF vs 56pF
TEXT 3352 280 Left 2 ;10pF vs 22pF vs 33pF vs 43pF vs 56pF
TEXT 4752 1496 Left 2 ;10p vs 0p
TEXT 4392 1616 Left 2 ;2.2p vs 10p vs 56p vs 110p vs 1n
TEXT 3296 1520 Left 2 ;10pF vs 22pF vs 43pF vs 56pF
TEXT 4256 1752 Left 2 ;1 / (2 * pi * (2.2 ohm) * (270 pF)) = 267.937615 megahertz\n1 / (2 * pi * (100 ohm) * (10 pF)) = 159.154943 megahertz\n1 / (2 * pi * (49.9 ohm) * (130 pF)) = 24.5344447 megahertz
TEXT 5384 2000 Left 2 ;10p vs 180p vs 270p vs 1000p
TEXT 5144 1872 Left 2 ;0.22 vs 499
TEXT 4736 2264 Left 2 ;10p vs 0p
TEXT 4376 2384 Left 2 ;2.2p vs 10p vs 56p vs 110p vs 1n
TEXT 3280 2288 Left 2 ;10pF vs 22pF vs 43pF vs 56pF
TEXT 4240 2520 Left 2 ;1 / (2 * pi * (2.2 ohm) * (270 pF)) = 267.937615 megahertz\n1 / (2 * pi * (100 ohm) * (10 pF)) = 159.154943 megahertz\n1 / (2 * pi * (49.9 ohm) * (130 pF)) = 24.5344447 megahertz
TEXT 5384 2784 Left 2 ;10p vs 180p vs 270p vs 1000p
TEXT 5144 2656 Left 2 ;0.22 vs 499
TEXT 4736 3048 Left 2 ;10p vs 0p vs 22p vs 330p
TEXT 4376 3168 Left 2 ;2.2p vs 10p vs 56p vs 110p vs 1n
TEXT 3280 3072 Left 2 ;10pF vs 22pF vs 43pF vs 56pF
TEXT 4240 3304 Left 2 ;1 / (2 * pi * (2.2 ohm) * (270 pF)) = 267.937615 megahertz\n1 / (2 * pi * (100 ohm) * (10 pF)) = 159.154943 megahertz\n1 / (2 * pi * (49.9 ohm) * (130 pF)) = 24.5344447 megahertz
TEXT 5808 3176 Left 2 ;1000p vs 180p vs 56p
