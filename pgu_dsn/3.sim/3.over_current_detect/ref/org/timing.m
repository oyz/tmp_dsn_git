close all;
clear;
format short e

mdata=load('ton50n.txt');

time=mdata(:,1);
out=mdata(:,2);
in=mdata(:,3);

l=length(time);

high=max(out);
low=min(out);
mid=(high+low)/2;

m=1;n=1;o=1;p=1;q=1;r=1;

for i=2:l
  if (in(i)-0.010000001)*(in(i-1)-0.010000001)<0
    tdInIndex(m)=i;
    m=m+1;
  endif
  if (out(i)-mid)*(out(i-1)-mid)<0
    tdOutIndex(n)=i;
    n=n+1;
  endif  
endfor

m1=round((tdOutIndex(1)+tdOutIndex(2))/2);
m2=round((tdOutIndex(3)+tdOutIndex(2))/2);

if out(m1)>out(m2)
  high=0.9*out(m1);
  low=0.9*out(m2);
else
  high=0.9*out(m2);
  low=0.9*out(m1);
endif
mid=(out(m1)+out(m2))/2;

n=1;
for i=2:l
  if (out(i)-mid)*(out(i-1)-mid)<0
    tdOutIndex(n)=i;
    n=n+1;
  endif  
  if (out(i)-low)*(out(i-1)-low)<0
    tOutLow(o)=time(i);
    outLow(o)=out(i);
    o=o+1;
  endif
  if (out(i)-high)*(out(i-1)-high)<0
    tOutHigh(p)=time(i);
    outHigh(p)=out(i);
    p=p+1;
  endif
endfor

for i=2:l
  if (in(i)-0.002)*(in(i-1)-0.002)<0
    tInLow(q)=time(i);
    inLow(q)=in(i);
    q=q+1;
  endif
  if (in(i)-0.018)*(in(i-1)-0.018)<0
    tInHigh(r)=time(i);
    inHigh(r)=in(i);
    r=r+1;
  endif
endfor

tdIn=time(tdInIndex);
dIn=in(tdInIndex);
tdOut=time(tdOutIndex);
dOut=out(tdOutIndex);
td=tdOut-tdIn

subplot(211)
plot(time,in,'r');
hold on
plot(tInLow,inLow,'ro');
plot(tInHigh,inHigh,'ro');
subplot(212)
plot(time,out,'b');
hold on
plot(tOutLow,outLow,'bo');
plot(tOutHigh,outHigh,'bo');

trIn=mean((tInHigh-tInLow)(tInHigh>tInLow));
tfIn=abs(mean((tInHigh-tInLow)(tInHigh<tInLow)));
trOut=mean((tOutHigh-tOutLow)(tOutHigh>tOutLow));
tfOut=abs(mean((tOutHigh-tOutLow)(tOutHigh<tOutLow)));
trDelta=trOut-trIn;
tfDelta=tfOut-tfIn;
%slewR=mean((out2-out1)(t2>t1)/(t2-t1)(t2>t1))
%slewF=abs(mean((out2-out1)(t2>t1)/(t2-t1)(t2<t1)))

ex=[td; trIn; tfIn; trOut; tfOut; trDelta; tfDelta]